/**************************************************************************************************\
*  Copyright (C) 2017
*  Mickle Somov
\**************************************************************************************************/

#ifndef OPENCL_H
#define OPENCL_H

#include <stdio.h>

#ifdef _WIN32
#include <windows.h>
#include <conio.h>
#include <direct.h>
#define GetCurrentDir _getcwd
static const char slash[] = "\\";
#else
#include <string.h>
#include <cstdlib>
#include <unistd.h>
#define GetCurrentDir getcwd
static const char slash[] = "/";

#define	sprintf_s(b,size,fmt,...) sprintf((b),(fmt),__VA_ARGS__)
#define	fopen_s(stream, buffer, mode) (*stream = fopen(buffer, mode))
//  #define	sscanf_s(buffer, format, value) (*value = sscanf(format, buffer))
#define	sscanf_s(buffer, format, value) (sscanf(buffer, format, value))
#define	_getch() (getchar())
#define	ctime_s(timebuf,timez,ltime) (timebuf=ctime(ltime))
#define	_strnset_s(Replace,replength,ch,replength2) (memset(Replace, ch, replength2))
#define	strncpy_s(newtext, replength, resorc, lenz) (strncpy(newtext, resorc, lenz))
#define	strcpy_s(newtext, replength, resorc) (strcpy(newtext, resorc))
#define	strcat_s(newtext, replength, resorc) (strcat(newtext, resorc))
//  #define	Sleep(tm) (sleep(tm/1000))
#define	Sleep(tm) (sleep(1))
#endif

#include <stdlib.h>
#include <math.h>
#include <assert.h>
#include <iostream>
#include <string>
#include <time.h>
#include <malloc.h>
#include <CL/cl.h>

class OpenCLTest
{
public:
	bool OpenCLOn;
	cl_event kernel_event, map_event;
	cl_int*  sortbuff;
	cl_int*  destbuff;
	cl_uint* glob;
	cl_int   CLError;
	char     infobuf[4096];
	cl_platform_id* platforms;
	cl_uint  platform_number;	// number of available platforms
	cl_uint  devices_number;		// number of available devices
	cl_bool  infobool;
	cl_uint  infouint;
	cl_ulong infoulong;
	cl_uint  infoitemdims;
	cl_platform_id platform;
	cl_platform_id platform2;
	cl_device_id   device;
	cl_device_local_mem_type local_mem_type;
	cl_context       context;
	cl_command_queue queue;
	cl_program       program;
	FILE     *prg_hndl;
	char     *prg_buffer, *prg_log;
	size_t    prg_size, log_size;
	cl_kernel kernel;
	cl_event event;
	size_t   wrk_nts_pr_krn;
	clock_t  timer;
public:
	bool initOpenCL();
	void OpenCLErr(cl_int CLerr, const char * CLerr_descr);
	void OpenCLGetPlatformIds();
	void NewContext();
	void CreateProgram(const char* file_name);
	void CreateKernel(const char* kernel_name);
	void NewQueue();
	void ReleaseAll();

	cl_uint clGetDeviceInfoUint(cl_device_id device, cl_device_info inf, const char* param_name);
	size_t clGetDeviceInfoSizeT(cl_device_id device, cl_device_info inf, const char* param_name);
	cl_ulong clGetDeviceInfoUlong(cl_device_id device, cl_device_info inf, const char* param_name);
	cl_bool clGetDeviceInfoBool(cl_device_id device, cl_device_info inf, const char* param_name);
};

#endif // END OF OPENCL_H
