#ifndef IMAGECL
#define IMAGECL

#define GID         (get_global_id(0) + get_global_id(1) * get_global_size(0))

__kernel void getRect( 
	__global float4 * input,
	__global float4 * cached,
	__global float4 * output,
	int4 rect,
	int widthSrc,
	int widthDst,
	int heightDst
)
{ 
	// get rectangle
	cached[GID] = input[get_global_id(0) + rect.x + (get_global_id(1) + rect.y) * widthSrc];

	if(widthDst < get_global_size(0) || heightDst < get_global_size(1))
	{
		barrier(CLK_GLOBAL_MEM_FENCE);
		int output_tid  = get_global_id(1) * widthDst + get_global_id(0);
 		
		float4 sum = {0.0f,0.0f,0.0f,0.0f};

		float x_ratio = (float)(get_global_size(0)/widthDst);
		float y_ratio = (float)(get_global_size(1)/heightDst);

		float area = x_ratio * y_ratio;

		int x_start   = (int)(get_global_id(0) * x_ratio);
		int x_end     = (int)(x_start + x_ratio);
		int y_start = (int)(get_global_id(1) * y_ratio);
		int y_end   = (int)(y_start + y_ratio);
			
		for(int y = y_start; y < y_end; ++y) {
			for(int x = x_start; x < x_end; ++x) {
				sum += cached[x + y*get_global_size(0)];
			}
		}
		output[output_tid] = sum / area;
	}
	
}

#endif
