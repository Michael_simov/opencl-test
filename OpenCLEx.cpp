/**************************************************************************************************\
*  Copyright (C) 2017
*  Mickle Somov
\**************************************************************************************************/
#include "OpenCLEx.h"

struct Rect
{
	int x, y, width, height;
};


void getRect(cl_float4* pixelsSrc, int widthSrc, int heightSrc, Rect rectSrc, cl_float4* pixelsDst, int widthDst, int heightDst)
{
	cl_int4 rect;
	clock_t time;
	const size_t global_size[] = { (const size_t)rectSrc.width, (const size_t)rectSrc.height }; //kernel global size
	const size_t local_size[] = { (const size_t)16 , (const size_t)16 };
	cl_float4* pixelsRect = (cl_float4*)calloc(rectSrc.width * rectSrc.height, sizeof(cl_float4)); // Rect buffer

	if (rectSrc.x + rectSrc.width > widthSrc || rectSrc.y + rectSrc.height > heightSrc)
	{
		printf("Wrong Rectangle offset or sizes\n");
		system("pause");
		exit(0);
	}

	OpenCLTest objectCL;
	objectCL.initOpenCL();
	objectCL.NewContext();
	objectCL.CreateProgram("image.cl");
	objectCL.CreateKernel("getRect");
	objectCL.NewQueue();
	cl_mem input_buff = clCreateBuffer(objectCL.context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(cl_float4)*widthSrc*heightSrc, pixelsSrc, &objectCL.CLError);
	objectCL.OpenCLErr(objectCL.CLError, "clCreateBuffer failed");
	objectCL.CLError = clSetKernelArg(objectCL.kernel, 0, sizeof(cl_mem), &input_buff);
	objectCL.OpenCLErr(objectCL.CLError, "clSetKernelArg failed");

	cl_mem rect_buff = clCreateBuffer(objectCL.context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, sizeof(cl_float4)*rectSrc.height*rectSrc.width, pixelsRect, &objectCL.CLError);
	objectCL.OpenCLErr(objectCL.CLError, "clCreateBuffer failed");
	objectCL.CLError = clSetKernelArg(objectCL.kernel, 1, sizeof(cl_mem), &rect_buff);
	objectCL.OpenCLErr(objectCL.CLError, "clSetKernelArg failed");

	cl_mem output_buff = clCreateBuffer(objectCL.context, CL_MEM_WRITE_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(cl_float4)*widthDst*heightDst, pixelsDst, &objectCL.CLError);
	objectCL.OpenCLErr(objectCL.CLError, "clCreateBuffer failed");
	objectCL.CLError = clSetKernelArg(objectCL.kernel, 2, sizeof(cl_mem), &output_buff);
	objectCL.OpenCLErr(objectCL.CLError, "clSetKernelArg failed");

	rect.s[0] = rectSrc.x;
	rect.s[1] = rectSrc.y;
	rect.s[2] = rectSrc.width;
	rect.s[3] = rectSrc.height;

	objectCL.CLError = clSetKernelArg(objectCL.kernel, 3, sizeof(cl_int4), &rect);
	objectCL.OpenCLErr(objectCL.CLError, "clSetKernelArg failed");

	objectCL.CLError = clSetKernelArg(objectCL.kernel, 4, sizeof(cl_int), &widthSrc);
	objectCL.OpenCLErr(objectCL.CLError, "clSetKernelArg failed");

	objectCL.CLError = clSetKernelArg(objectCL.kernel, 5, sizeof(cl_int), &widthDst);
	objectCL.OpenCLErr(objectCL.CLError, "clSetKernelArg failed");

	objectCL.CLError = clSetKernelArg(objectCL.kernel, 6, sizeof(cl_int), &heightDst);
	objectCL.OpenCLErr(objectCL.CLError, "clSetKernelArg failed");
	printf("Run sampling\n"); 

	time = clock();
	objectCL.CLError = clEnqueueNDRangeKernel(objectCL.queue, objectCL.kernel, 2, NULL, global_size, local_size, 0, NULL, &objectCL.event);
	time = clock() - time;

	objectCL.OpenCLErr(objectCL.CLError, "clEnqueueNDRangeKernel failed");
	printf("\nKernel time: %0.3f sec\n\n", (double) time / CLK_TCK);

	clEnqueueReadBuffer(objectCL.queue, rect_buff, CL_TRUE, 0, sizeof(cl_float4)*rectSrc.width*rectSrc.height, pixelsRect, 0, NULL, NULL);

	if (widthDst < rectSrc.width || heightDst < rectSrc.height) 
	{
		clEnqueueReadBuffer(objectCL.queue, output_buff, CL_TRUE, 0, sizeof(cl_float4)*widthDst*heightDst, pixelsDst, 0, NULL, NULL);
	}
	else 
	{
		clEnqueueReadBuffer(objectCL.queue, rect_buff, CL_TRUE, 0, sizeof(cl_float4)*widthDst*heightDst, pixelsDst, 0, NULL, NULL);
	}

	//objectCL.CLError = clWaitForEvents(1, &objectCL.event);
	//objectCL.OpenCLErr(objectCL.CLError, "clWaitForEvents failed");
	objectCL.CLError = clFinish(objectCL.queue);
	objectCL.OpenCLErr(objectCL.CLError, "clFinish failed");
	clReleaseMemObject(input_buff);
	clReleaseMemObject(rect_buff);
	clReleaseMemObject(output_buff);
	objectCL.ReleaseAll();
}

void sampleImage() 
{
	int widthSrc = 1024*5; // Input image width 
	int heightSrc = 1024*5; // Input image height

	Rect rectSrc = { 20, 110, 1024, 1024 }; // Rectangle: x, y, width, height

	int widthDst = 900; // Output image width
	int heightDst = 900; // Output image height
	
	cl_float4* pixelsSrc = (cl_float4*) calloc ( widthSrc * heightSrc, sizeof(cl_float4)); // Input image buffer with a four float per pixel
	cl_float4* pixelsDst = (cl_float4*)calloc(widthDst * heightDst, sizeof(cl_float4)); // Output image will be cropped from input buffer
	
	for (size_t i = 0; i < widthSrc * heightSrc; i++) {
		pixelsSrc[i].s[0] = 1.f;
		pixelsSrc[i].s[1] = (float) (rand()) / (float)(RAND_MAX);
		pixelsSrc[i].s[2] = (float) (rand()) / (float)(RAND_MAX);
		pixelsSrc[i].s[3] = (float) (rand()) / (float)(RAND_MAX);
	}
	
	getRect(pixelsSrc, widthSrc, heightSrc, rectSrc, pixelsDst, widthDst, heightDst); 
}

int main()
{
	sampleImage();
	system("pause");
}
