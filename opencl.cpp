/**************************************************************************************************\
*  Copyright (C) 2017
*  Mickle Somov
\**************************************************************************************************/
#include "opencl.h"

bool OpenCLTest::initOpenCL()
{
	OpenCLOn = false;
	platform = NULL;
	device = NULL;

	OpenCLErr(clGetPlatformIDs(0, NULL, &platform_number), "clGetPlatformIDs failed");

	if (platform_number == 0) {
		printf("There are no any available OpenCL platforms\n");
		return false;
	}

	platforms = new cl_platform_id[platform_number];
	OpenCLErr(clGetPlatformIDs(platform_number, platforms, NULL), "clGetPlatformIDs failed");
	for (unsigned int i = 0; i < platform_number; i++)
	{
		platform = platforms[i];
		OpenCLErr(clGetDeviceIDs(platform, CL_DEVICE_TYPE_ALL, 0, 0, &devices_number), "clGetDeviceIDs failed");
		cl_device_id* devices = new cl_device_id[devices_number];
		OpenCLErr(clGetDeviceIDs(platform, CL_DEVICE_TYPE_ALL, devices_number, devices, &devices_number), "clGetDeviceIDs failed");
		for (unsigned int t = 0; t<devices_number; t++)
		{
			cl_device_type device_type;
			OpenCLErr(clGetDeviceInfo(devices[t], CL_DEVICE_TYPE, sizeof(device_type), &device_type, NULL), "clGetDeviceInfo failed");
			if (device_type == CL_DEVICE_TYPE_GPU)
			{
				device = devices[t];
				
				size_t parameter_length;
				printf("\nPlatform: ");
				OpenCLErr(clGetPlatformInfo(platform, CL_PLATFORM_NAME, 0, NULL, &parameter_length), "clGetPlatformInfo failed");
				char* str = (char*)calloc(parameter_length, sizeof(char));
				OpenCLErr(clGetPlatformInfo(platform, CL_PLATFORM_NAME, (parameter_length), str, NULL), "clGetPlatformInfo failed");
				printf("%s\n", str);
				printf("Device:   ");
				OpenCLErr(clGetDeviceInfo(device, CL_DEVICE_NAME, 0, NULL, &parameter_length), "clGetDeviceInfo failed");
				str = (char*)calloc(parameter_length, sizeof(char));
				OpenCLErr(clGetDeviceInfo(device, CL_DEVICE_NAME, (parameter_length), str, NULL), "clGetDeviceInfo failed");
				printf("%s\n", str);
				free(str);

				OpenCLOn = true;
				break;
			}
		}
		delete[] devices;
	}

	delete[] platforms;

	return OpenCLOn;
}

void OpenCLTest::OpenCLErr(cl_int CLerr, const char * CLerr_descr)
{
	if (CLerr != CL_SUCCESS) {
		printf("ERROR %i: (%s)\n", CLerr, CLerr_descr);
		system("pause");
		CLError = CLerr;
		exit(1);
	}
}

void OpenCLTest::OpenCLGetPlatformIds()
{
	OpenCLErr(clGetPlatformIDs(0, NULL, &this->platform_number), "clGetPlatformIDs failed");
}

void OpenCLTest::NewContext()
{
	if (!device)
	{
		OpenCLErr(CL_DEVICE_NOT_FOUND, "device not found");
	}

	context = clCreateContext(NULL, 1, &device, NULL, NULL, &CLError);
	OpenCLErr(CLError, "clCreateContext failed");
}

void OpenCLTest::CreateProgram(const char* file_name)
{
	
#ifdef WIN32
	if ( fopen_s(&prg_hndl, file_name, "r") != 0) {
#else
	if ((prg_hndl = fopen_s(file_name, "r")) == NULL) {
#endif
		printf("Cannot find or open cl file\n");
		system("pause");
		exit(1);
	}
	fseek(prg_hndl, 0, SEEK_END);
	prg_size = ftell(prg_hndl);
	//fseek(prg_hndl, 0, SEEK_SET);
	printf("Program size: %d\n", (int)prg_size);
	rewind(prg_hndl);
	prg_buffer = (char*)calloc(prg_size + 1, sizeof(char));
	prg_buffer[prg_size] = '\0';
	fread(prg_buffer, sizeof(char), prg_size, prg_hndl);
	fclose(prg_hndl);

	program = clCreateProgramWithSource(context, 1, (const char**)&prg_buffer, &prg_size, &CLError);

	OpenCLErr(CLError, "clCreateProgramWithSource failed");
	CLError = clBuildProgram(program, 0, NULL, NULL, NULL, NULL);
	OpenCLErr(CLError, "clBuildProgram failed");
}

void OpenCLTest::CreateKernel(const char* kernel_name)
{
	kernel = clCreateKernel(program, kernel_name, &CLError);
	OpenCLErr(CLError, "clCreateKernel failed");
}

void OpenCLTest::NewQueue()
{
	if (!device)
	{
		OpenCLErr(CL_DEVICE_NOT_FOUND, "device not found");
	}
	if (!context)
	{
		OpenCLErr(CL_INVALID_CONTEXT, "invalid or not found context");
	}
	queue = clCreateCommandQueue(context, device, CL_QUEUE_PROFILING_ENABLE, &CLError);
	OpenCLErr(CLError, "clCreateCommandQueue failed");
}

void OpenCLTest::ReleaseAll()
{
	clReleaseEvent(event);
	clReleaseKernel(kernel);
	clReleaseProgram(program);
	clReleaseCommandQueue(queue);
	clReleaseContext(context);
}
